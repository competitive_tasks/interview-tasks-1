class Mapper:
    map_0 = {
        1: "один",
        2: "два",
        3: "три",
        4: "четыре",
        5: "пять",
        6: "шесть",
        7: "семь",
        8: "восемь",
        9: "девять",
    }

    map_1 = {
        1: "десять",
        2: "двадцать",
        3: "тридцать",
        4: "сорок",
        5: "пятьдесят",
        6: "шестьдесят",
        7: "семьдесят",
        8: "восемьдесят",
        9: "девяноста",
    }

    map_2 = {
        1: "сто",
        2: "двести",
        3: "триста",
        4: "четыреста",
        5: "пятьсот",
        6: "шестьсот",
        7: "семьсот",
        8: "восемьсот",
        9: "девятьсот",
    }


def pronounce_number(number: int):
    i = 0
    result = []
    while number > 0:
        n = number % 10
        if n != 0:
            result.append(getattr(Mapper, f"map_{i}")[n])
        number = number // 10
        i += 1

    result.reverse()

    return " ".join(result)


def sound_phone(phone: str):
    items = phone.split('-')
    res = [pronounce_number(int(el)) for el in items]
    return " - ".join(res)


def task_1():
    nums = list(map(int, input().split(' ')))

    l_p = nums[0]
    i = 1
    while i < len(nums):
        diff = nums[i] - nums[i - 1]
        if diff == 1:
            # continue
            pass
        elif diff == 0:
            # bad [2 3 3 4...]
            if l_p != nums[i - 1]:
                print(l_p, '-', nums[i - 1])
                i += 1
            else:
                print(nums[i - 1])
            l_p = nums[i]
        else:
            # bad [2 3 5 6]
            print(l_p, '-', nums[i - 1])
            l_p = nums[i]
            i += 1

        i += 1

    if l_p == nums[-1]:
        print(l_p)
    else:
        print(l_p, '-', nums[-1])


if __name__ == '__main__':
    res = sound_phone("8-800-555-35-35")
    print(res)
